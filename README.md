# Hangman

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Deployment status
[![Netlify Status](https://api.netlify.com/api/v1/badges/4b5d7851-4d58-467f-b99e-893cdf99232f/deploy-status)](https://app.netlify.com/sites/vue-hangman-project/deploys)
